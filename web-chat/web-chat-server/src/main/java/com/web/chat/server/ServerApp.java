package com.web.chat.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class ServerApp {
    private static final int PORT = 23;

    public static void main(String[] args) throws Exception{
        //only deals with connecting clients. 1 thread to connect the clients is enough here
        EventLoopGroup bossGroup =  new NioEventLoopGroup(1);
        //processing interaction
        EventLoopGroup workGroup = new NioEventLoopGroup();
        try{
            //start the server
            ServerBootstrap sb = new ServerBootstrap();
            sb.group(bossGroup, workGroup)
            //we need channel
            .channel(NioServerSocketChannel.class)
            .childHandler(new ChannelInitializer<SocketChannel>() {

                protected void initChannel(SocketChannel socketChannel) throws Exception {
                    //pipeline - for processing messages
                    //add handler to the conveyor process
                    socketChannel.pipeline().addLast(new Handler());

            }

            });
            //we want server to start in our PORT
            //ChannelFuture - information about the opened connection
            ChannelFuture cf = sb.bind(PORT).sync();
            System.out.println("Server is started. Waiting for clients");
            // while server is working we are waiting, if it is closed we will close our threads
            cf.channel().closeFuture().sync();

        }finally{
            //shutting down the threads
            bossGroup.shutdownGracefully();
            workGroup.shutdownGracefully();
        }

    }
}

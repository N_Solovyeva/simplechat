package com.web.chat.server;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class Handler extends ChannelInboundHandlerAdapter {

    //this method will work when the client connects
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
        System.out.println("New client connected");
    }
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        super.channelRead(ctx, msg);
        //ByteBuf is just a byte storage
        ByteBuf bb = (ByteBuf)msg;
        //while we have something to read
        while(bb.readableBytes()>0){
            //we will read it byte by byte and print in the console
            // and something goes wrong here!!!
            // ERROR
            try {
                System.out.print((char) bb.readByte());
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        //release buffer
        bb.release();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        super.exceptionCaught(ctx, cause);
        ctx.close();
        cause.printStackTrace();
    }
}
